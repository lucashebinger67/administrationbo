/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.atlantis.administrationbo;

import javax.persistence.*;
import org.junit.*;

/**
 *
 * @author administrator
 */
public class Insertion {
    
    // Interface used to work with the persistence context
    private static EntityManager entityManager;

    // Used to control entityManager, once entityManagerFactory is closed, every entityManager are closed too
    private static EntityManagerFactory entityManagerFactory;

    // Used to create an entityManager and entityManagerFactory with the persistenceUnitName "userPu" created in persistence.xml
    @BeforeClass
    public static void setUpEntityManagerFactory() {
        entityManagerFactory = Persistence.createEntityManagerFactory("UserPu");
        entityManager = entityManagerFactory.createEntityManager();
    }

    // Used to close an entityManager and entityManagerFactory
    @AfterClass
    public static void closeEntityManagerFactory() {
        entityManager.close();
        entityManagerFactory.close();
    }

    // Method to test the persistence
    @Test
    public void persistUser() {

        EntityManager entityManager = entityManagerFactory.createEntityManager();
        entityManager.getTransaction().begin();

        // We create a new object "User"
        User lucas = new User("Lucas", 22, 0);

        // Inserting into the MongoDB
        entityManager.persist(lucas);
        entityManager.getTransaction().commit();

        // Closing the entityManager
        entityManager.close();

    }
    
}